package com.te.ems.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.te.ems.dto.Employee;
@Repository
public interface EmployeeDao extends CrudRepository<Employee, String>{
	
	public Employee findByEmailaddress(String emailaddress);
	
	public Employee findByPassword(String password);
	
	

}
