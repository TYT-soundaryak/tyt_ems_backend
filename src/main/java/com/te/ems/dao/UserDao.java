package com.te.ems.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.te.ems.dto.User;

@Repository
public interface UserDao extends CrudRepository<User,String> {

}
