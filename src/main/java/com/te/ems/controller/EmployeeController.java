package com.te.ems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.te.ems.dto.Employee;
import com.te.ems.dto.User;
import com.te.ems.service.EmployeeService;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeService service;
	
	@PostMapping("/signup")
	public ResponseEntity<?> signup(@RequestBody User user){
		try {
		service.usersignup(user);
		return new ResponseEntity<String>("Registered SuccessFully",HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<String>(" Unable to register !Please try after sometime",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> validation(String emailaddress,String password) {
		try{
			service.login(emailaddress, password);
			return new ResponseEntity<String>("login Successfully",HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<String>("please check your creditionals",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> add(@RequestBody Employee employee){
		try {
		service.add(employee);
		return new ResponseEntity<String>("Added SuccessFully",HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<String>(" Unable to !Please try after sometime",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> edit(@RequestBody Employee employee){
		try {
		service.edit(employee);
		return new ResponseEntity<String>("Edited SuccessFully",HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<String>(" Unable to Edit!!!Please try after sometime",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping("/view")
	public List<Employee> viewEmployee(){
		return service.viewaddedemployee();
	}
	
	@DeleteMapping("/delete")
	public ResponseEntity<?>delete(String emailaddress){
		
		try {
			service.delete(emailaddress);
			return new ResponseEntity<String>("deleted Successfully!!",HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<String>("Unable delete the deatils!!",HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	
	

}
