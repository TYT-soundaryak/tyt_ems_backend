package com.te.ems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.ems.dao.EmployeeDao;
import com.te.ems.dao.UserDao;
import com.te.ems.dto.Employee;
import com.te.ems.dto.User;

@Service
public class EmployeeServiceImplimentation implements EmployeeService{
	
	@Autowired
	private EmployeeDao employeedao;
	
	@Autowired
	private UserDao userdao;

	@Override
	public Employee add(Employee employee) {
		return employeedao.save(employee);
		
		}

	@Override
	public User usersignup(User user) {
		return userdao.save(user);
	}

	@Override
	public Employee edit(Employee employee) {
		
		return employeedao.save(employee);
	}

	@Override
	public List<Employee> viewaddedemployee() {
		
		List<Employee> list=(List<Employee>)employeedao.findAll();
		return list;
	}

	@Override
	public boolean login(String emailaddress, String password) {
		
		Employee emp=employeedao.findByEmailaddress(emailaddress);

		if (emp.getPassword().equals(password)){
			return true;
		}
		return false;
	}

	@Override
	public void delete(String emailaddress) {
		Employee employee=employeedao.findByEmailaddress(emailaddress);
		employeedao.delete(employee);
		
		
	}

}
