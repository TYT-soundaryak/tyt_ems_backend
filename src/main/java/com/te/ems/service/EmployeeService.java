package com.te.ems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.ems.dao.EmployeeDao;
import com.te.ems.dto.Employee;
import com.te.ems.dto.User;


public interface EmployeeService {
	
	public Employee add(Employee employee);
	
	public User usersignup(User user);
	
	public Employee edit(Employee employee);
	
	public List<Employee> viewaddedemployee();
	
	public boolean login(String emailaddress,String password);
	
	public void delete(String emailaddress);
	

}
