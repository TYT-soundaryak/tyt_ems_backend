package com.te.ems.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Value;

import lombok.Data;

@Data
@Entity
public class Employee implements Serializable{
	
	@Id
	@Column
	private String emailaddress;
	@Column
	private String fullname;
	@Column
	private String Designation;
	@Column
	private  double salary;
	@Column
	private int age;
	@Column
	private String password;
}
