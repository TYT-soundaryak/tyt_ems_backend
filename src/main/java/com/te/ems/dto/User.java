package com.te.ems.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class User implements Serializable {
	@Id
	@Column
	private String emailaddress;
	@Column
	private String name;
	@Column
	private String password;
	@Column
	private String username;
	

}
